//
//  RegisterViewController.swift
//  UBER-EPN
//
//  Created by Alex on 11/19/19.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseStorage
import FirebaseAuth

class RegisterViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate {

    //UINavigationControllerDelegate cuando se ejecute aparecera como transicion, tiene metodos delef¡gados que regresan a la pnatalla anterior
    //UIImagePickerControllerDelegate Da una notificacion donde viene la imagen que se seeccinna
    
    //MARK: - Attributes
    
    @IBOutlet weak var firstnameTextField: UITextField!
    @IBOutlet weak var lastnameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var birthDateTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var pictureImageView: UIImageView!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    
    let birthDatePicker = UIDatePicker()
    var imagePicker = UIImagePickerController ()
    
    //MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        birthDateTextField.delegate = self
        birthDatePicker.datePickerMode = .date
        
        //self hace referencia a la clase, que valor se va a ejecutar y cuando se va a aejecutar
        birthDatePicker.addTarget(self, action: #selector(dateValueChanged(_:)), for: .valueChanged)
        
        activityView.hidesWhenStopped=true
        activityView.isHidden=true

    }
    //MARK: - Logic
    
    @IBAction func pictureButtonPressed(_ sender: Any) {
        let actionSheet = UIAlertController(title: "Add Picture", message: nil, preferredStyle: .actionSheet)
        
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default){
            (_)
            in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        
        
        let libraryAction = UIAlertAction(title: "Library", style: .default){
            (_)in
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .savedPhotosAlbum
                self.imagePicker.allowsEditing = true
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(libraryAction)
        
        present(actionSheet, animated: true, completion: nil )
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        pictureImageView.image = info [.editedImage] as! UIImage
        picker.dismiss(animated: true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 10{
            textField.inputView = birthDatePicker
        }
    }
    

    @objc func dateValueChanged(_ sender: UIDatePicker) {
        let format = DateFormatter()
        format.dateFormat = "dd-MM-YYYY"
        birthDateTextField.text = format.string(from: birthDatePicker.date)
    }
    
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        activityView.isHidden=false
        activityView.startAnimating()
        
         createUser { userUID in
            self.uploadImage(userUID: userUID) { imageURL in
            self.saveToFireStore(imageURL: imageURL, userUID: userUID) {
                
                self.activityView.stopAnimating()
                  // go to next screen
                self.performSegue(withIdentifier: "registerSuccessSegue", sender: self)
                }
              }
            }
    }
    
    func uploadImage (userUID: String, completionHandler: @escaping (String) -> ()){
        //let firestore = Firestore.firestore() //inicializo

              let storage = Storage.storage()
              
              let ref = storage.reference()
              let profileImage = ref.child("users")
                            
              let userPicture = profileImage.child("\(userUID).jpg")
              
              let data = pictureImageView.image?.jpegData(compressionQuality: 0.5)! //en pictureImageView se tiene guardada la imagen y la funcion que se le da le permite convertir a base 64. ESTO PARA IMAGENES DE NO ALTA CALIDAD
              
              let _ = userPicture.putData(data!, metadata: nil){(metadata, error) in
                
                if error != nil {
                    self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
                }
                
                  guard let _ = metadata else {
                      return
                  }
                  
                  userPicture.downloadURL{(url, error) in
                    if error != nil {
                        self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
                    }
                      guard let downloadURL = url else {
                          return
                      }
                    completionHandler(downloadURL.absoluteString)
                  }
              }
    }
    
    //func saveToFireStore(userUID: String, imageURL: String, completionHandler: @escaping ()->() ){
    //}
    
    func saveToFireStore (imageURL: String, userUID : String, completionHandler : @escaping () -> ()){
        let firestore = Firestore.firestore() //inicializo

        firestore.collection("users").document(userUID).setData( [
            "name": self.firstnameTextField.text ?? "",
            "lastName": self.lastnameTextField.text ?? "",
            "email": self.emailTextField.text ?? "",
            "birthdate": self.birthDateTextField.text ?? "",
            "phone": self.phoneTextField.text ?? "",
            "imageURL": "\(imageURL)"
            
        ]){(error)in
            if error != nil {
                self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
            }
            if error == nil{
                completionHandler()
            }
        }
        //dentro del String se pone el path de nuestro archivo
    }
    
    //salida de un hilo
    func createUser (completionHandler: @escaping (String) -> ()) { //completionHandler , escaping nos deja salir del hilo
        
        let auth = Auth.auth()
        auth.createUser(withEmail: emailTextField.text!, password: passwordTextField.text!){
            (authInfo, error) in
            if error != nil {
                self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
            }
            if error == nil {
                completionHandler(authInfo!.user.uid)
            }
        }
    }
    
    func showAlert(title: String, message: String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alertController.addAction(okAlertAction)
        present(alertController, animated: true, completion: nil)
    }
    
    
    
    
    
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
}

//
//  RideViewController.swift
//  UBER-EPN
//
//  Created by Alex on 12/17/19.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit
import MapKit
import Firebase

class RideViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    
    let locationManager = CLLocationManager()
    let firestore = Firestore.firestore() //inicializo

    var annotation: [String: (MKPointAnnotation, UIColor)] = [:]

    
    override func viewDidLoad() {
        super.viewDidLoad()

        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy=kCLLocationAccuracyNearestTenMeters
        locationManager.distanceFilter=kCLDistanceFilterNone //se puede ver la distancia a la que se va a encontrar los taxis
        locationManager.startUpdatingLocation() //muestra el puntito donde esta parado
        //location consume mucha bateria
        mapView.showsUserLocation = true
        locationManager.delegate = self
        // Do any additional setup after loading the view.
        centerMapIn(location: locationManager.location)
        getAllUsers()
        mapView.delegate = self //para que cada usuario tenga un color
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations:[CLLocation]){
        //centerMapIn(location: locations[locations.count - 1])
        let lastLocation = locations[locations.count - 1].coordinate
        let lat = lastLocation.latitude.description
        let long = lastLocation.longitude.description
        firestore.collection("location")
            .document(String(Auth.auth().currentUser!.uid))
            .setData([
                "coordinates": [lat, long],
                "color": [235.0/255, 127.0/255, 20.0/255]
            ])
    }
    
    func centerMapIn (location:CLLocation?){ //con esta funcion voy a hacer que se entre en esa posicion y tenga un radio de 10000 km
        guard let location = location else {
            return
        }
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 10000, longitudinalMeters: 10000)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    //hacer un Query para recuperar todos los documentos y leer esa info
    func getAllUsers(){
        firestore
        .collection("location")
            .getDocuments{(snapshot, error)in
                
                //hacer una funcion que si no encuentra documentos, entonces de error
                if error != nil{
                    print(error)
                    return
                }
                //Si esq encuentra funciones...
                for document in snapshot!.documents{
                    self.showUser(id: document.documentID)
                }
                
        }
    }
    //con la funcion showUser vamos a pintar a los compañeros en el mapa
    func showUser (id: String){
        //si esq el id q intenta mostrar es el mismo mio, entonces no muestre nada
        if id == String (Auth.auth().currentUser!.uid){
            return
        }
        //crear un anotation o pin q se pone sobre el mapa
        
        //1
        let annotation = UberAnnotation()
        
        firestore
        .collection("location")
        .document(id)
        .addSnapshotListener { (snapshot, error) in
            if error != nil {
                print(error)
                return
            }
        
        let colorData = snapshot?.get("color") as! Array<Double>
        let rgbColor = colorData.map {CGFloat($0) }
        annotation.userColor = UIColor(red : rgbColor [0], green: rgbColor [1], blue: rgbColor [2], alpha: 1)
            annotation.userId = id
            
            self.mapView.addAnnotation(annotation)
            //self.mapview.addAnnotation(annotation)
        }
        
        
        //2
        //mapView.addAnnotation(annotation) //se le agrega al mapa
        //annotation.userId = id
        
        firestore
            .collection("location")
            .document(id)
            .addSnapshotListener{ (snapshot, error)in
                if error != nil{
                    print (error)
                    return
                }
                //cada que se muevve se hace un cambio al mapa
                let data = snapshot?.get("coordinates") as! Array<String>
                let coordinates = data.map { Double($0)!}
                
                let lat = coordinates [0]
                let long = coordinates [1]
                
                let annotationCoordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
                annotation.coordinate = annotationCoordinate
                
                //let colorData = snapshot?.get("coordinates") as! Array <String>
                //let rgbColor = colorData.map{
                   // CGFloat(Double($0) ?? 0)
                //} // hacer un casting a double y despues se le pasa a CGFloat
                //annotation.userColor = UIColor(red: rgbColor[0], green: rgbColor[1], blue: rgbColor[2], alpha: 1)
        }
    }
    //por cada funcion que agregamos al mapa, esta funcion nos ayuda a dibujarlo
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
      if annotation is MKUserLocation{
        return nil;
      }else{
        let customAnnotation = annotation as! UberAnnotation
        //print(customAnnotation.userId)
        //print(customAnnotation.userColor)
        let pinIdent = "Pin";
        var pinView: UberAnnotationView;
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: pinIdent) as? UberAnnotationView {
          dequeuedView.annotation = customAnnotation;
          pinView = dequeuedView;
        }else{
            pinView = UberAnnotationView(annotation: customAnnotation, reuseIdentifier: pinIdent, image: nil, color: customAnnotation.userColor);
        }
        //pinView.pinTintColor = customAnnotation.userColor
        return pinView;
      }
    }
    
    @IBAction func centerLocationPressed(_ sender: Any) {
        centerMapIn(location: locationManager.location)
    }
    

}

//
//  UberAnnotationView.swift
//  UBER-EPN
//
//  Created by Alex on 1/7/20.
//  Copyright © 2020 Alex. All rights reserved.
//

import Foundation
import MapKit

class UberAnnotation: MKPointAnnotation {
    var userId: String?
    var userColor: UIColor?
}

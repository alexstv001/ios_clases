//
//  UberAnnotationView.swift
//  UBER-EPN
//
//  Created by Alex on 1/10/20.
//  Copyright © 2020 Alex. All rights reserved.
//

import Foundation
import MapKit

class UberAnnotationView: MKAnnotationView{
    var imageView = UIImageView()
    var color:UIColor
    let annotationFrame = CGRect(x: 0, y: 0, width: 40, height: 40) //el centro del pin donde vamos a poner la imagen
    
    init(annotation: MKAnnotation?, reuseIdentifier: String?, image: UIImage?, color:UIColor?) { //annotation y reuseIdentifier se necesita para llamar al constructor padre
        self.color = color ?? UIColor.black
        imageView.image = image
        imageView.contentMode = .scaleAspectFill
        imageView.frame = annotationFrame
        
        imageView.backgroundColor = color
        imageView.layer.cornerRadius = 20
        imageView.layer.masksToBounds = true
        
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier) //llamada al constructor de la clase padre
        
        addSubview(imageView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

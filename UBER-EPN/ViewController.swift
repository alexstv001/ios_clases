//
//  ViewController.swift
//  UBER-EPN
//
//  Created by Alex on 11/8/19.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController {

    @IBOutlet weak var emaiTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        if let _ = Auth.auth().currentUser{
            performSegue(withIdentifier: "loginSuccessSegue", sender: self)
        }
    }

    @IBAction func LoginButton(_ sender: Any) {
        guard let email = emaiTextField.text, let password = passwordTextField.text else {
                return
            }
            Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
                    //print("RESULT: \(result)")
                //print("ERROR: \(error)")
                
                if error != nil {
                    self.presentAlertWith(title: "Error", message: error?.localizedDescription ?? "Ups! An error ocurred")
                } else{
                    self.performSegue(withIdentifier: "loginSuccessSegue", sender: self)
                }
            }
        }
    
    private func presentAlertWith(title: String, message: String){
        let alertControlller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAlertAcction = UIAlertAction(title: "OK", style: .default) {
            _ in
            self.emaiTextField.text = ""
            self.passwordTextField.text = ""
            
            self.emaiTextField.becomeFirstResponder()
            
        }
        
        alertControlller.addAction(okAlertAcction)
        
        present(alertControlller, animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {//funcion que se ejecuta cada vez que tocamos la pantalla
        //emaiTextField.resignFirstResponder()
        //passwordTextField.resignFirstResponder()
        
        view.endEditing(true)//todo lo que este editandose se va a cerrar
    }
}
